module.exports = {
  purge: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      fontFamily: {
        body: 'Nunito',
        title: 'Inter',
      },
      colors: {
        muted: '#98a6ad',
        body: '#6c757d',
        gray: {
          primary: '#c8cbcf',
          secondary: '#dee2e6',
        },
        sidebar: {
          primary: '#313a46',
          body: '#8391a2',
          'body-secondary': '#bccee4',
        },
      },
      backgroundImage: {
        'auth-background': 'url("../img/abstract_background.jpg")',
      },
      height: {
        4.5: '18px',
        5.5: '22px',
        6.5: '26px',
      },
      zIndex: {
        1: '1px',
      },
      fontSize: {
        md: '15px',
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
