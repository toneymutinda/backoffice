import { createSlice } from '@reduxjs/toolkit'

const initialState = {
  user: {},
  isAuthenticating: false,
}

export const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    setUser: (state, action) => {
      state.user = action.payload
    },
    setIsAuthenticating: (state, action) => {
      state.isAuthenticating = action.payload
    },
  },
})

export const { setUser, setIsAuthenticating } = authSlice.actions

export default authSlice.reducer
