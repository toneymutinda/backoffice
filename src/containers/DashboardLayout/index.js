import PropTypes from 'prop-types'
import { Sidebar, Header } from '../../components'

function DashboardLayout({ children }) {
  return (
    <div className='flex flex-row min-h-screen'>
      <Sidebar />

      <main className='bg-gray-100 flex flex-col flex-grow'>
        <Header />
        <div className='p-4'>{children}</div>
      </main>
    </div>
  )
}

DashboardLayout.propTypes = {
  children: PropTypes.object.isRequired,
}

export default DashboardLayout
