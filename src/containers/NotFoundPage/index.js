import { useHistory } from 'react-router'
import { icons } from '../../assets/img'
import { Button } from '../../components'
import { DASHBOARD } from '../App/RouteConstants'

function NotFoundPage() {
  const history = useHistory()

  return (
    <div className='flex items-center h-screen justify-center'>
      <div className='text-center'>
        <img src={icons.icon404} alt='404' className='w-auto h-auto mx-auto' />
        <h4 className='font-title font-bold text-xl mb-3'>Page Not Found</h4>
        <p className='mb-3'>We {`couldn't`} find the page you are looking for.</p>

        <Button
          type='primary'
          label='Back to Dashboard'
          styles={{ fontWeight: 'bold', textTransform: 'uppercase' }}
          onClick={() => history.push(DASHBOARD)}
        />

        <p className='text-muted absolute bottom-0 py-4'>
          2019 - {new Date().getFullYear()} &copy; Bluehat - All rights reserved
        </p>
      </div>
    </div>
  )
}

export default NotFoundPage
