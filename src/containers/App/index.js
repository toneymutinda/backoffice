import { Provider } from 'react-redux'
import { PropTypes } from 'prop-types'
import Routes from './Routes'

const App = ({ store }) => (
  <Provider store={store}>
    <Routes />
  </Provider>
)

App.propTypes = {
  store: PropTypes.object.isRequired,
}

export default App
