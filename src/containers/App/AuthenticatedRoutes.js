import { Suspense, lazy } from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'
import PropTypes from 'prop-types'
import DashboardLayout from '../DashboardLayout'
import { FallBackLoader } from '../../components'
import { DASHBOARD, INDEX } from './RouteConstants'

const DashboardRoute = ({ element, ...rest }) => (
  <Route
    {...rest}
    render={(props) => <DashboardLayout {...props}>{element}</DashboardLayout>}
  />
)

const NotFoundPage = lazy(() => import('../NotFoundPage'))
const OverviewPage = lazy(() => import('../OverviewPage'))

DashboardRoute.propTypes = {
  element: PropTypes.element.isRequired,
}

function AuthenticatedRoutes() {
  return (
    <Suspense fallback={FallBackLoader}>
      <Switch>
        <DashboardRoute element={<OverviewPage />} exact path={DASHBOARD} />
        <Redirect exact from={INDEX} to={DASHBOARD} />
        <Route component={NotFoundPage} />
      </Switch>
    </Suspense>
  )
}

export default AuthenticatedRoutes
