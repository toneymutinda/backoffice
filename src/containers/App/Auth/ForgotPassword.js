import { Link } from 'react-router-dom'
import { Input, FormLabel, FormGroup, Button } from '../../../components'
import AuthLayout from '../../../layouts/auth'
import { LOGIN } from '../RouteConstants'

function ForgotPassword() {
  return (
    <AuthLayout
      leftColumn={
        <div>
          <div className='font-bold mb-3 text-base text-center sm:text-left'>
            Reset Password
          </div>
          <div className='help-text text-sm mb-4 text-center sm:text-left'>
            Enter your email address and {`we'll`} send you an email with instructions to
            reset your password.
          </div>

          <FormGroup>
            <FormLabel text='Email address' />
            <Input type='text' placeholder='Enter your email' onChange={() => null} />
          </FormGroup>

          <Button label='Send Reset Password Link' />

          <div className='text-center mt-9 text-sm'>
            <span>Back to</span>
            <Link to={LOGIN} className='ml-1 no-underline text-blue-600 font-semibold'>
              Log in
            </Link>
          </div>
        </div>
      }
      rightColumn={<div></div>}
    />
  )
}

export default ForgotPassword
