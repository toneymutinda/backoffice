import { Link } from 'react-router-dom'
import { Input, FormLabel, FormGroup, Button } from '../../../components'
import AuthLayout from '../../../layouts/auth'
import { LOGIN } from '../RouteConstants'

function ResetPassword() {
  return (
    <AuthLayout
      leftColumn={
        <div>
          <div className='font-bold mb-3 text-base text-center sm:text-left'>
            Confirm Password Reset
          </div>
          <div className='help-text text-sm mb-4 text-center sm:text-left'>
            Enter your new password and remember to keep it safe.
          </div>

          <FormGroup>
            <FormLabel text='Password' />
            <Input type='text' placeholder='Enter your password' onChange={() => null} />
          </FormGroup>

          <FormGroup>
            <FormLabel text='Confirm Password' />
            <Input
              type='text'
              placeholder='Confirm your password'
              onChange={() => null}
            />
          </FormGroup>

          <Button label='Reset Password' />

          <div className='text-center mt-9 text-sm'>
            <span>Back to</span>
            <Link to={LOGIN} className='ml-1 no-underline text-blue-600 font-semibold'>
              Log in
            </Link>
          </div>
        </div>
      }
      rightColumn={<div></div>}
    />
  )
}

export default ResetPassword
