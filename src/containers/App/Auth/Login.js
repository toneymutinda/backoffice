import { Link } from 'react-router-dom'
import { Input, FormLabel, FormGroup, Button } from '../../../components'
import AuthLayout from '../../../layouts/auth'
import { FORGOT_PASSWORD } from '../RouteConstants'

function Login() {
  return (
    <AuthLayout
      leftColumn={
        <div>
          <div className='font-bold mb-3 text-base text-center sm:text-left'>Sign In</div>
          <div className='help-text text-sm mb-4 text-center sm:text-left'>
            Enter your email address and password to continue.
          </div>

          <FormGroup>
            <FormLabel text='Email address' />
            <Input type='text' placeholder='Enter your email' onChange={() => null} />
          </FormGroup>

          <FormGroup>
            <FormLabel text='Password' />
            <Input type='text' placeholder='Enter your password' onChange={() => null} />
          </FormGroup>

          <FormGroup>
            <Input type='checkbox' onChange={() => null} checkboxLabel='Remember me' />
          </FormGroup>

          <Button label='Sign In' />

          <div className='text-center mt-9 text-sm'>
            <span>Forgot your password?</span>
            <Link
              to={FORGOT_PASSWORD}
              className='ml-1 no-underline text-blue-600 font-semibold'
            >
              Click here
            </Link>
          </div>
        </div>
      }
      rightColumn={<div></div>}
    />
  )
}

export default Login
