import { Suspense, lazy } from 'react'
import { FallBackLoader } from '../../components'

const AuthenticatedRoutes = lazy(() => import('./AuthenticatedRoutes'))

export default function DashBoardRoutes() {
  return (
    <>
      <Suspense fallback={<FallBackLoader />}>
        <AuthenticatedRoutes />
      </Suspense>
    </>
  )
}
