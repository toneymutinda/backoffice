import { Suspense, lazy } from 'react'
import { Route, BrowserRouter, Switch } from 'react-router-dom'
import { FallBackLoader } from '../../components'
import { FORGOT_PASSWORD, LOGIN, RESET_PASSWORD } from './RouteConstants'

const LoginPage = lazy(() => import('./Auth/Login'))
const NotFoundPage = lazy(() => import('../NotFoundPage'))
const ForgotPasswordPage = lazy(() => import('./Auth/ForgotPassword'))
const ResetPasswordPage = lazy(() => import('./Auth/ResetPassword'))
const DashboardRoutes = lazy(() => import('./DashboardRoutes'))

export default function AppRoutes() {
  return (
    <Suspense fallback={<FallBackLoader />}>
      <BrowserRouter>
        <Switch>
          <Route exact path={LOGIN} component={LoginPage} />
          <Route exact path={FORGOT_PASSWORD} component={ForgotPasswordPage} />
          <Route exact path={RESET_PASSWORD} component={ResetPasswordPage} />
          <Route component={DashboardRoutes} />
          <Route component={NotFoundPage} />
        </Switch>
      </BrowserRouter>
    </Suspense>
  )
}
