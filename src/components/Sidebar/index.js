import { Link } from 'react-router-dom'
import { DASHBOARD } from '../../containers/App/RouteConstants'

function Sidebar() {
  return (
    <aside className='w-64 bg-sidebar-primary text-sidebar-body hidden sm:block'>
      {/* Sidebar Header */}
      <div className='flex items-center justify-center py-4'>
        <div className='inline-flex'>
          <Link className='inline-flex flex-row items-center' to={DASHBOARD}>
            <span className='leading-10 font-title text-lg font-black text-white ml-1 uppercase'>
              LOGO
            </span>
          </Link>
        </div>
      </div>

      {/* Sidebar Content */}
      <div className='px-4 py-6'>
        <ul className='flex flex-col w-full'>
          <li className='my-px text-md text-white transition ease-in-out duration-400 pb-2'>
            <Link className='flex flex-row items-center px-3'>
              <icon className='bi bi-house mr-3 text-lg'></icon>
              <span className='align-middle'>Dashboard</span>
            </Link>
          </li>
        </ul>
      </div>
    </aside>
  )
}

export default Sidebar
