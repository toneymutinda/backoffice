import { useEffect } from 'react'
import PropTypes from 'prop-types'

function Notification({ type, message, show, duration, updateNotificationState }) {
  const getNotificationType = (notificationType) => {
    let classes
    switch (notificationType) {
      case 'error':
        classes = 'bg-red-200 border border-red-400 text-red-700'
        break
      case 'primary':
        classes = 'bg-blue-200 border border-blue-400 text-blue-700'
        break
      case 'success':
        classes = 'bg-green-200 border border-green-400 text-green-700'
        break
      case 'warning':
        classes = 'bg-yellow-200 border border-yellow-400 text-yellow-700'
        break
      default:
        break
    }

    return classes
  }

  useEffect(() => {
    let timer

    if (duration === 'short') {
      timer = setTimeout(() => {
        updateNotificationState({ show: false })
      }, 3000)
    } else if (duration === 'long') {
      timer = setTimeout(() => {
        updateNotificationState({ show: false })
      }, 5000)
    } else if (duration === 'very long') {
      timer = setTimeout(() => {
        updateNotificationState({ show: false })
      }, 7000)
    } else if (duration === 'indefinite') {
      return
    }

    return () => {
      clearTimeout(timer)
    }
  })

  return show ? (
    <div
      className={`${getNotificationType(
        type
      )} fixed z-50 text-center p-4 bottom-4 rounded flex`}
    >
      {message}
    </div>
  ) : null
}

Notification.propTypes = {
  type: PropTypes.string,
  message: PropTypes.string.isRequired,
  show: PropTypes.bool,
  duration: PropTypes.string,
  updateNotificationState: PropTypes.func.isRequired,
}

Notification.defaultProps = {
  type: 'primary',
  show: false,
  duration: 'indefinite',
}

export default Notification
