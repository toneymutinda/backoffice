import PropTypes from 'prop-types'

function Input({ onChange, type, placeholder, hasError, checkboxLabel }) {
  if (type === 'checkbox') {
    return (
      <label className='inline-flex items-center'>
        <input
          type={type}
          className='text-blue-600 w-4 h-4 mr-2 focus:ring-indigo-400 focus:ring-opacity-25 outline-none rounded'
          onChange={onChange}
        />
        {checkboxLabel}
      </label>
    )
  } else {
    return (
      <input
        type={type}
        className={`w-full block leading-6 bg-clip-padding rounded py-2 px-4 border ${
          hasError
            ? 'border-red-300 focus:border-red-400'
            : 'border-gray-secondary focus:border-gray-primary'
        } transition ease-in-out duration-200 focus:outline-none bg-white`}
        placeholder={placeholder}
        onChange={onChange}
      />
    )
  }
}

Input.propTypes = {
  onChange: PropTypes.func.isRequired,
  placeholder: PropTypes.string,
  hasError: PropTypes.bool,
  type: PropTypes.string,
  checkboxLabel: PropTypes.string,
}

Input.defaultProps = {
  type: 'text',
}

export default Input
