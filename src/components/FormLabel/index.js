import PropTypes from 'prop-types'

function FormLabel({ text }) {
  return <div className='text-sm font-semibold mb-3'>{text}</div>
}

FormLabel.propTypes = {
  text: PropTypes.string.isRequired,
}

export default FormLabel
