import { icons } from '../../assets/img'

function FallBackLoader() {
  return (
    <section className='h-screen flex items-center justify-center'>
      <img src={icons.iconLoaderOne} alt='loader' className='animate-spin' />
    </section>
  )
}

export default FallBackLoader
