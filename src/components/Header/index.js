import { Fragment, useState } from 'react'
import { Menu, Transition } from '@headlessui/react'
// import { Link } from 'react-router-dom'
import { icons } from '../../assets/img'
import { Link } from 'react-router-dom'

function classNames(...classes) {
  return classes.filter(Boolean).join(' ')
}

const languages = [
  {
    label: 'English',
    value: 'en',
    icon: icons.iconUsFlag,
  },
  {
    label: 'German',
    value: 'de',
    icon: icons.iconGermanFlag,
  },
  {
    label: 'French',
    value: 'fr',
    icon: icons.iconFrenchFlag,
  },
  {
    label: 'Spanish',
    value: 'es',
    icon: icons.iconSpainFlag,
  },
  {
    label: 'Italian',
    value: 'it',
    icon: icons.iconItalyFlag,
  },
]

function Header() {
  const [activeLanguage, setActiveLanguage] = useState('en')

  return (
    <header
      className='bg-white w-full shadow-sm z-50 top-0 right-0 px-4 flex items-stretch'
      style={{ minHeight: '70px' }}
    >
      <div className='flex items-center flex-no-shrink'>
        <i className='bi bi-list sm:hidden inline-block cursor-pointer text-gray-900 text-2xl'></i>
        <div className='relative'>
          <span className='absolute inset-y-0 left-3 flex items-center'>
            <i className='bi bi-search text-gray-400 text-lg'></i>
          </span>
          <div className='flex'>
            <input
              type='text'
              className='rounded-tr-none rounded-br-none rounded-tl-md rounded-bl-md bg-gray-100 shadow-sm pl-10 pr-5 py-2 focus:outline-none'
              placeholder='Search...'
            />
            <button className='cursor-pointer bg-blue-600 shadow-sm text-sm px-4 py-2 text-white rounded-tr-md rounded-br-md'>
              Search
            </button>
          </div>
        </div>
      </div>
      <div className='flex items-center ml-auto'>
        <div className='w-56 text-right'>
          <Menu as='div' className='relative inline-block text-left mr-6'>
            <div>
              <Menu.Button className='inline-flex justify-center items-center w-full'>
                <img
                  src={languages.filter((lang) => lang.value === activeLanguage)[0].icon}
                  alt={`${
                    languages.filter((lang) => lang.value === activeLanguage)[0].label
                  } Flag`}
                  className='mr-2 w-5'
                />
                {languages.filter((lang) => lang.value === activeLanguage)[0].label}
                <i className='bi bi-chevron-down -mr-1 ml-2' aria-hidden='true'></i>
              </Menu.Button>
            </div>

            <Transition
              as={Fragment}
              enter='transition ease-out duration-100'
              enterFrom='transform opacity-0 scale-95'
              enterTo='transform opacity-100 scale-100'
              leave='transition ease-in duration-75'
              leaveFrom='transform opacity-100 scale-100'
              leaveTo='transform opacity-0 scale-95'
            >
              <Menu.Items className='origin-top-right absolute right-0 mt-6 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5 w-37 py-1 focus:outline-none w-52'>
                <div className='py-1'>
                  {languages
                    .filter((lang) => lang.value !== activeLanguage)
                    .map((language) => (
                      <Menu.Item key={language.value}>
                        {({ active }) => (
                          <button
                            onClick={() => setActiveLanguage(language.value)}
                            className={classNames(
                              active ? 'bg-gray-100 text-gray-900' : 'text-gray-700',
                              'group flex items-center w-full px-2 py-2 text-sm'
                            )}
                          >
                            <img
                              src={language.icon}
                              alt={`${language.label} Flag`}
                              className='w-5 mr-2'
                            />
                            {language.label}
                          </button>
                        )}
                      </Menu.Item>
                    ))}
                </div>
              </Menu.Items>
            </Transition>
          </Menu>
        </div>
        <div className='text-right'>
          <Menu as='div' className='relative inline-block text-left mr-6'>
            <div>
              <Menu.Button className='w-full flex items-center'>
                <img src={icons.iconAvatar} alt='Avatar' className='w-9' />
                <span className='ml-2'>
                  <span className='block font-bold text-sm'>Toney Mutinda</span>
                  <span className='text-xs float-left'>System Admin</span>
                </span>
              </Menu.Button>
            </div>

            <Transition
              as={Fragment}
              enter='transition ease-out duration-100'
              enterFrom='transform opacity-0 scale-95'
              enterTo='transform opacity-100 scale-100'
              leave='transition ease-in duration-75'
              leaveFrom='transform opacity-100 scale-100'
              leaveTo='transform opacity-0 scale-95'
            >
              <Menu.Items className='origin-top-right absolute right-0 mt-5 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5 w-37 py-1 focus:outline-none w-48 divide-y divide-gray-100'>
                <div className='py-1'>
                  <Menu.Item>
                    {({ active }) => (
                      <Link
                        to='/'
                        className={classNames(
                          active ? 'bg-gray-100 text-gray-900' : 'text-gray-700',
                          'group flex items-center w-full px-2 py-2 text-sm'
                        )}
                      >
                        <i className='bi bi-person-circle mr-2 text-xl'></i>
                        Profile
                      </Link>
                    )}
                  </Menu.Item>
                  <Menu.Item>
                    {({ active }) => (
                      <Link
                        to='/'
                        className={classNames(
                          active ? 'bg-gray-100 text-gray-900' : 'text-gray-700',
                          'group flex items-center w-full px-2 py-2 text-sm'
                        )}
                      >
                        <i className='bi bi-gear mr-2 text-xl'></i>
                        Account Settings
                      </Link>
                    )}
                  </Menu.Item>
                </div>
                <div className='py-1'>
                  <Menu.Item>
                    {({ active }) => (
                      <Link
                        to='/'
                        className={classNames(
                          active ? 'bg-gray-100 text-gray-900' : 'text-gray-700',
                          'group flex items-center w-full px-2 py-2 text-sm'
                        )}
                      >
                        <i className='bi bi-box-arrow-right mr-2 text-xl'></i>
                        Logout
                      </Link>
                    )}
                  </Menu.Item>
                </div>
              </Menu.Items>
            </Transition>
          </Menu>
        </div>
      </div>
    </header>
  )
}

export default Header
