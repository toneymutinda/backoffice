import PropTypes from 'prop-types'

function Button({ label, type, styles, onClick }) {
  const getButtonType = (buttonType) => {
    let btnClasses = 'bg-blue-600 hover:bg-blue-700'
    switch (buttonType) {
      case 'danger':
        btnClasses = 'bg-red-600 hover:bg-red-700'
        break
      default:
        break
    }

    return btnClasses
  }

  return (
    <button
      className={`${getButtonType(
        type
      )} border border-transparent text-center rounded py-2 px-4 text-white w-full transition ease-in-out duration-200 focus:outline-none`}
      style={styles}
      onClick={onClick}
    >
      {label}
    </button>
  )
}

Button.propTypes = {
  label: PropTypes.string.isRequired,
  type: PropTypes.string,
  styles: PropTypes.object,
  onClick: PropTypes.func,
}

Button.defaultProps = {
  type: 'primary',
}

export default Button
