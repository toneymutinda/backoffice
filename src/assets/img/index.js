export const icons = {
  iconLogin: require('./login_illustration.svg').default,
  iconAbstractBackground: require('./abstract_background.jpg').default,
  iconParty: require('./party.svg').default,
  icon404: require('./404.png').default,
  iconLoaderOne: require('./loader_one.png').default,
  iconUsFlag: require('./us.svg').default,
  iconGermanFlag: require('./de.svg').default,
  iconFrenchFlag: require('./fr.svg').default,
  iconItalyFlag: require('./it.svg').default,
  iconSpainFlag: require('./es.svg').default,
  iconAvatar: require('./avatar.png').default,
}
