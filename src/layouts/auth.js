import PropTypes from 'prop-types'

const AuthLayout = ({ leftColumn, rightColumn }) => {
  return (
    <section className='sm:grid sm:grid-cols-3 block'>
      <div className='col-span-1 h-screen flex items-center justify-center px-8 bg-white'>
        {leftColumn}
      </div>
      <div
        className='col-span-2 px-12 items-center text-center h-screen hidden justify-center sm:flex bg-auth-background bg-opacity-30'
        style={{ background: '' }}
      >
        {rightColumn}
      </div>
    </section>
  )
}

AuthLayout.propTypes = {
  leftColumn: PropTypes.node.isRequired,
  rightColumn: PropTypes.node.isRequired,
}

export default AuthLayout
