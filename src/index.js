import { StrictMode } from 'react'
import ReactDOM from 'react-dom'
import './assets/css/index.css'
import 'bootstrap-icons/font/bootstrap-icons.css'
import App from './containers/App'
import reportWebVitals from './reportWebVitals'
import { store } from './store'

ReactDOM.render(
  <StrictMode>
    <App store={store} />
  </StrictMode>,
  document.getElementById('root')
)

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals()
